#
#  Be sure to run `pod spec lint ISWebServiceKit.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

  s.name         = "WebServiceKit"
  s.version      = "1.0.6"
  s.summary      = "This framework provides the server communication"

  s.description  = "This will assist to establish server communication"

  s.homepage     = "https://dev_sincotechnology@bitbucket.org/dev_sincotechnology/webservicekit"

  s.license      = "MIT"

  s.author             = { "Idris Jovial SOP NWABO" => "idrisjovisop@gmail.com" }
  s.platform     = :ios, "14.0"

  s.source       = { :git => "https://dev_sincotechnology@bitbucket.org/dev_sincotechnology/webservicekit.git", :tag => s.version.to_s }
  

  s.source_files  = "WebServiceKit/**/*"

end
