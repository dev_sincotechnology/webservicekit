//
//  WebServiceKit.h
//  WebServiceKit
//
//  Created by Idris SOP on 2019/03/21.
//  Copyright © 2019 Idris SOP. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WebServiceKit.
FOUNDATION_EXPORT double WebServiceKitVersionNumber;

//! Project version string for WebServiceKit.
FOUNDATION_EXPORT const unsigned char WebServiceKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WebServiceKit/PublicHeader.h>

